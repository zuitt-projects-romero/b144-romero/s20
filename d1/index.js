//JSON OBJECTS
//JSON stands for JavaScript Object Notation
//JSON is used for serializing different data types into bytes.
//Serializtion is the process of converting data into a series of bytes for easier transmission/transfer of information
//byte =? binary digits (1 and 0) that is used to represent a character

/*
JSON data format
Syntax:
	{
		"propertyA": "valueA",
		"propertyB": "valueB"
	}
*/ 


/*
JS Object
{
	city: "QC",
	province: "Metro Manila",
	country: 'Philippines'
}


JSON 
{
	'city': "QC",
	"province": "Metro Manila",
	"country": "Philippines"
}


JSON ARRAY
*/

/*"cities": [
	{
		'city': "QC",
		"province": "Metro Manila",
		"country": "Philippines"
	},
	{
		'city': "QC",
		"province": "Metro Manila",
		"country": "Philippines"
	},
	{
		'city': "QC",
		"province": "Metro Manila",
		"country": "Philippines"
	}

]*/


//JSON METHODS
//The JSON contains methods for parsing and converting data into stringified JSON

/*
- Stringified JSON is a javascript object converted into a string to be used in other function of a Javascript application
*/

let batchesArr = [
	{
		batchName: 'Batch X'
	},
	{
		batchName: 'Batch Y'
	}
]

console.log('Result from stringify method')

//The 'stringify' method is used to convert JS Objects into JSON(string)
console.log(JSON.stringify(batchesArr))


let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})

console.log(data)

/*
Mini-Activity
Use the prompt method in order to gather user data to be supplied to the user details, then once the data gathered, convert it into stringify and print the details
//User details:

firstName:
lastName:
age:
address: city, country, zip code
*/

let firstName = prompt("First Name: ")
let lastName = prompt("Last Name: ")
let age = prompt("Age: ")
let address = [prompt("City: "), prompt("Country: "), prompt("Zip Code: ")]


let userDetails = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(userDetails)

//Converting Stringified JSON into js objects
//Information is commonly sent to applications in stringified JSON and then converted back into objects.
//This happens both for sending information to a backend app and sending information back to frontend app
//Upon receiving data, JSON text can be converted to a JS object with parse

let batchesJSON = `[
	{
		"batchName": "Batch X"
	},
	{
		"batchName": "Batch Y"
	}

]`

console.log('Result from parse method:')
console.log(JSON.parse(batchesJSON))


let stringifiedObject = `
	{
		"name": "John",
		"age": "31",
		"address": {
			"city": "Manila",
			"country": "Philippines"
		}
	}
`

console.log(JSON.parse(stringifiedObject))
